import { useState, useEffect, FC } from "react";
import { Button, Checkbox } from "@chakra-ui/react";
import { FaBolt } from "react-icons/fa";
import { localGetItem, localSetItem } from "@utils/storage";

type Props = {
  onAuto: () => void,
  [key: string]: any
};

const initLocalStorage = () => {
  const initial = localGetItem("isauto");
  return initial ? initial === "true" : true;
};

const AutoTranslateButton: FC<Props> = ({ onAuto, ...props }) => {
  const [isAuto, setIsAuto] = useState(initLocalStorage);

  useEffect(() => {
    localSetItem("isauto", isAuto.toString());
  }, [isAuto]);

  useEffect(() => {
    isAuto && onAuto();
  }, [isAuto, onAuto]);

  const onclick = () => setIsAuto(current => !current);

  return (
    <Button
      aria-label="Switch auto"
      icon={<FaBolt />}
      onClick={onclick}
      colorScheme="lingva"
      variant="outline"
      {...props}
    >
      <Checkbox
        isChecked={isAuto}
        onChange={onclick}
      >
        Auto Translate
      </Checkbox>
    </Button>
  );
};

export default AutoTranslateButton;
