import { FC } from "react";
import { HiTranslate } from "react-icons/hi";
import { Button } from "@chakra-ui/react";

type Props = {
  onClick: () => void,
  [key: string]: any
};

const TranslateButton: FC<Props> = ({ onClick, ...props }) => {
  return <>
    <Button
      aria-label="Translate"
      onClick={onClick}
      colorScheme="lingva"
      variant="solid"
      w={["full", null, "auto"]}
      {...props}
    >
      <HiTranslate />
      Translate
    </Button>
  </>
}

export default TranslateButton;

