import { FC } from "react";
import Head from "next/head";
import NextLink from "next/link";
import { Flex, HStack, IconButton, Link, useColorModeValue } from "@chakra-ui/react";
import { FaGitAlt } from "react-icons/fa";
import Image from "next/image";
import { ColorModeToggler } from ".";

type Props = {
  [key: string]: any
};

const Header: FC<Props> = (props) => (
  <>
    <Head>
      <link rel="prefetch" href="/banner_light.svg" />
      <link rel="prefetch" href="/banner_dark.svg" />
    </Head>

    <Flex
      as="header"
      px={[8, null, 24, 40]}
      py={3}
      justify="space-between"
      align="center"
      w="full"
      {...props}
    >
      <NextLink href="/" passHref={true}>
        <Link
          display="flex"
          placeItems="center"
          _hover={{
            textDecoration: "none"
          }}
          fontWeight="bold"
          fontSize="xl"
        >
          <Image
            src={useColorModeValue("/banner_light.svg", "/banner_dark.svg")}
            alt="Logo"
            width={70}
            height={64}
          />
          <span>
            LingMe
          </span>
        </Link>
      </NextLink>
      <HStack spacing={3}>
        <ColorModeToggler
          variant={useColorModeValue("outline", "solid")}
        />
        <IconButton
          as={Link}
          href="https://gitlab.com/Wilmhit/lingme-translate"
          isExternal={true}
          aria-label="Git Repository"
          icon={<FaGitAlt />}
          colorScheme="lingva"
          variant={useColorModeValue("outline", "solid")}
        />
      </HStack>
    </Flex>
  </>
);

export default Header;
