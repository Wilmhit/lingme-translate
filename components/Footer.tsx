import { FC } from "react";
import { Stack, Text, Link } from "@chakra-ui/react";

type Props = {
  [key: string]: any
};


const Footer: FC<Props> = (props) => (
  <Stack
    as="footer"
    w="full"
    p={3}
    fontSize={["sm", null, "md"]}
    direction={["column", null, "row"]}
    justify="center"
    align="center"
    spacing={[1, null, 2]}
    {...props}
  >
    <Link href="https://copyheart.org/" isExternal={true}>
      <Text as="span">♡ Copying is an act of love.</Text>
    </Link>
    <Text as="span" display={["none", null, "unset"]}>·</Text>
    <Link href="https://www.gnu.org/licenses/agpl-3.0.html" isExternal={true}>
      <Text as="span">Licensed under AGPLv3</Text>
    </Link>
  </Stack>
);

export default Footer;
