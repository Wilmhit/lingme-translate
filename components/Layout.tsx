import { FC } from "react";
import { Flex, VStack, useColorModeValue } from "@chakra-ui/react";
import { Header, Footer } from ".";

type Props = {
  [key: string]: any
};

const Layout: FC<Props> = ({ children, ...props }) => (
  <>
    <VStack minH="100vh" spacing={8}>
      <Header
        bgColor={useColorModeValue("lingva.100", "lingva.900")}
      />

      <Flex
        as="main"
        id="main"
        flexGrow={1}
        w="full"
        {...props}
      >
        {children}
      </Flex>

      <Footer
        bgColor={useColorModeValue("lingva.100", "lingva.900")}
        color={useColorModeValue("lingva.900", "lingva.100")}
      />
    </VStack>
  </>
);

export default Layout;
